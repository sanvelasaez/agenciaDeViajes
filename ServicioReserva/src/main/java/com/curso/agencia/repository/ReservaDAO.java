package com.curso.agencia.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.curso.agencia.entity.Reserva;

/**
 * Clase DAO para la gestion de Reserva mediante JPA
 * 
 * @author Santiago Velasco
 *
 */
@Repository
public interface ReservaDAO extends JpaRepository<Reserva, Integer>{

}
