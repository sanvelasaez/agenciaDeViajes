package com.curso.agencia.service;

import java.util.List;

import com.curso.agencia.entity.Reserva;
import com.curso.agencia.model.ObjetosCombinadosDTO;

/**
 * Interfaz servicios del microservicio ServicioReserva
 * 
 * @author Santiago Velasco
 *
 */
public interface ReservaService {

	/**
	 * 
	 * @param reserva       objeto reserva para ser insertado en la base de datos
	 * @param totalPersonas cantidad de personas para modificar los asientos
	 *                      disponibles en el vuelo
	 */
	void hacerReserva(Reserva reserva, Integer totalPersonas);

	/**
	 * 
	 * @return listado conjunto de las reservas con hoteles y vuelos
	 */
	List<ObjetosCombinadosDTO> listarReservas();

}
