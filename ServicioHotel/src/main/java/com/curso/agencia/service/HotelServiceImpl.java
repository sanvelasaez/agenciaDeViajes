package com.curso.agencia.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.curso.agencia.entity.Hotel;
import com.curso.agencia.repository.HotelDAO;

/**
 * Clase de servicios del microservicio ServicioHotel
 * 
 * @author Santiago Velasco
 *
 */
@Service
public class HotelServiceImpl implements HotelService {

	@Autowired
	HotelDAO dao;

	@Override
	public List<Hotel> listarHotelesDisponibles() {
		return dao.listarHotelesDisponibles();
	}

	@Override
	public Hotel buscarPorNombre(String nombre) {
		for (Hotel hotel : dao.findAll()) {
			if (hotel.getNombre().equals(nombre)) {
				return hotel;
			}
		}
		return null;
	}

	@Override
	public Hotel buscarPorId(Integer idHotel) {
		return dao.findById(idHotel).orElse(null);
	}

}
