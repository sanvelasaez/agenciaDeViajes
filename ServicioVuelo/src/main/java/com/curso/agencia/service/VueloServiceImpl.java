package com.curso.agencia.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.curso.agencia.entity.Vuelo;
import com.curso.agencia.repository.VueloDAO;

/**
 * Clase de servicios del microservicio ServicioVuelo
 * 
 * @author Santiago Velasco
 *
 */
@Service
public class VueloServiceImpl implements VueloService {

	@Autowired
	VueloDAO dao;

	@Override
	public List<Vuelo> filtrarPorPlazas(Integer plazasDisponibles) {
		return dao.filtrarPorPlazas(plazasDisponibles);
	}

	@Override
	public void actualizarPlazas(Integer idVuelo, Integer plazasReservadas) {
		Vuelo vuelo = dao.findById(idVuelo).orElse(null);
		if (vuelo != null) {
			Integer plazasRestantes = vuelo.getPlazasDisponibles() - plazasReservadas;
			vuelo.setPlazasDisponibles(plazasRestantes);
			dao.save(vuelo);
		}
	}
	
	public Vuelo buscaVuelo(Integer idVuelo) {
		return dao.findById(idVuelo).orElse(null);
	}

}
