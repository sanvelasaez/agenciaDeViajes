package com.curso.agencia.model;

import java.sql.Timestamp;

import com.curso.agencia.entity.Reserva;

/**
 * Clase encargada de combinar las clases de Reserva, Hotel y Vuelo para poder
 * mostrar todos los datos de manera comoda
 * 
 * @author Santiago Velasco
 *
 */
public class ObjetosCombinadosDTO {

	private Integer idReserva;
	private String nombreCliente;
	private String dni;
	private Integer idVuelo;
	private String companiaVuelo;
	private Timestamp fechaVuelo;
	private Double precioVuelo;
	private Integer plazasDisponiblesVuelo;
	private Integer idHotel;
	private String nombrehotel;
	private String categoriaHotel;
	private Double precioHotel;
	private Boolean disponibleHotel;

	/**
	 * 
	 * @param reserva datos de la reserva
	 * @param vuelo   datos del vuelo de la reserva
	 * @param hotel   datos del hotel de la reserva
	 */
	public ObjetosCombinadosDTO(Reserva reserva, Vuelo vuelo, Hotel hotel) {
		this.idReserva = reserva.getIdReserva();
		this.nombreCliente = reserva.getNombreCliente();
		this.dni = reserva.getDni();
		this.idVuelo = vuelo.getIdVuelo();
		this.companiaVuelo = vuelo.getCompania();
		this.fechaVuelo = vuelo.getFechaVuelo();
		this.precioVuelo = vuelo.getPrecio();
		this.plazasDisponiblesVuelo = vuelo.getPlazasDisponibles();
		this.idHotel = hotel.getIdHotel();
		this.nombrehotel = hotel.getNombre();
		this.categoriaHotel = hotel.getCategoria();
		this.precioHotel = hotel.getPrecio();
		this.disponibleHotel = hotel.getDisponible();
	}

	/**
	 * @return the idReserva
	 */
	public Integer getIdReserva() {
		return idReserva;
	}

	/**
	 * @param idReserva the idReserva to set
	 */
	public void setIdReserva(Integer idReserva) {
		this.idReserva = idReserva;
	}

	/**
	 * @return the nombreCliente
	 */
	public String getNombreCliente() {
		return nombreCliente;
	}

	/**
	 * @param nombreCliente the nombreCliente to set
	 */
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	/**
	 * @return the dni
	 */
	public String getDni() {
		return dni;
	}

	/**
	 * @param dni the dni to set
	 */
	public void setDni(String dni) {
		this.dni = dni;
	}

	/**
	 * @return the idVuelo
	 */
	public Integer getIdVuelo() {
		return idVuelo;
	}

	/**
	 * @param idVuelo the idVuelo to set
	 */
	public void setIdVuelo(Integer idVuelo) {
		this.idVuelo = idVuelo;
	}

	/**
	 * @return the companiaVuelo
	 */
	public String getCompaniaVuelo() {
		return companiaVuelo;
	}

	/**
	 * @param companiaVuelo the companiaVuelo to set
	 */
	public void setCompaniaVuelo(String companiaVuelo) {
		this.companiaVuelo = companiaVuelo;
	}

	/**
	 * @return the fechaVuelo
	 */
	public Timestamp getFechaVuelo() {
		return fechaVuelo;
	}

	/**
	 * @param fechaVuelo the fechaVuelo to set
	 */
	public void setFechaVuelo(Timestamp fechaVuelo) {
		this.fechaVuelo = fechaVuelo;
	}

	/**
	 * @return the precioVuelo
	 */
	public Double getPrecioVuelo() {
		return precioVuelo;
	}

	/**
	 * @param precioVuelo the precioVuelo to set
	 */
	public void setPrecioVuelo(Double precioVuelo) {
		this.precioVuelo = precioVuelo;
	}

	/**
	 * @return the plazasDisponiblesVuelo
	 */
	public Integer getPlazasDisponiblesVuelo() {
		return plazasDisponiblesVuelo;
	}

	/**
	 * @param plazasDisponiblesVuelo the plazasDisponiblesVuelo to set
	 */
	public void setPlazasDisponiblesVuelo(Integer plazasDisponiblesVuelo) {
		this.plazasDisponiblesVuelo = plazasDisponiblesVuelo;
	}

	/**
	 * @return the idHotel
	 */
	public Integer getIdHotel() {
		return idHotel;
	}

	/**
	 * @param idHotel the idHotel to set
	 */
	public void setIdHotel(Integer idHotel) {
		this.idHotel = idHotel;
	}

	/**
	 * @return the nombrehotel
	 */
	public String getNombrehotel() {
		return nombrehotel;
	}

	/**
	 * @param nombrehotel the nombrehotel to set
	 */
	public void setNombrehotel(String nombrehotel) {
		this.nombrehotel = nombrehotel;
	}

	/**
	 * @return the categoriaHotel
	 */
	public String getCategoriaHotel() {
		return categoriaHotel;
	}

	/**
	 * @param categoriaHotel the categoriaHotel to set
	 */
	public void setCategoriaHotel(String categoriaHotel) {
		this.categoriaHotel = categoriaHotel;
	}

	/**
	 * @return the precioHotel
	 */
	public Double getPrecioHotel() {
		return precioHotel;
	}

	/**
	 * @param precioHotel the precioHotel to set
	 */
	public void setPrecioHotel(Double precioHotel) {
		this.precioHotel = precioHotel;
	}

	/**
	 * @return the disponibleHotel
	 */
	public Boolean getDisponibleHotel() {
		return disponibleHotel;
	}

	/**
	 * @param disponibleHotel the disponibleHotel to set
	 */
	public void setDisponibleHotel(Boolean disponibleHotel) {
		this.disponibleHotel = disponibleHotel;
	}

}
