package com.curso.agencia.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad hotel para la gestion por JPA de los mismos
 * 
 * @author Santiago Velasco
 *
 */
@Entity
@Table(name = "reserva")
public class Reserva {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idReserva;
	private String nombreCliente;
	private String dni;
	private Integer idVuelo;
	private Integer idHotel;
	/**
	 * @param idReserva 	identificador de la reserva
	 * @param nombreCliente	nombre del cliente que crea la reserva
	 * @param dni			dni del cliente que crea la reserva
	 * @param idVuelo		identificador del vuelo reservado
	 * @param idHotel		identificador del hotel reservado
	 */
	public Reserva(Integer idReserva, String nombreCliente, String dni, Integer idVuelo, Integer idHotel) {
		this.idReserva = idReserva;
		this.nombreCliente = nombreCliente;
		this.dni = dni;
		this.idVuelo = idVuelo;
		this.idHotel = idHotel;
	}
	/**
	 * 
	 */
	public Reserva() {
	}
	/**
	 * @return the idReserva
	 */
	public Integer getIdReserva() {
		return idReserva;
	}
	/**
	 * @param idReserva the idReserva to set
	 */
	public void setIdReserva(Integer idReserva) {
		this.idReserva = idReserva;
	}
	/**
	 * @return the nombreCliente
	 */
	public String getNombreCliente() {
		return nombreCliente;
	}
	/**
	 * @param nombreCliente the nombreCliente to set
	 */
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	/**
	 * @return the dni
	 */
	public String getDni() {
		return dni;
	}
	/**
	 * @param dni the dni to set
	 */
	public void setDni(String dni) {
		this.dni = dni;
	}
	/**
	 * @return the idVuelo
	 */
	public Integer getIdVuelo() {
		return idVuelo;
	}
	/**
	 * @param idVuelo the idVuelo to set
	 */
	public void setIdVuelo(Integer idVuelo) {
		this.idVuelo = idVuelo;
	}
	/**
	 * @return the idHotel
	 */
	public Integer getIdHotel() {
		return idHotel;
	}
	/**
	 * @param idHotel the idHotel to set
	 */
	public void setIdHotel(Integer idHotel) {
		this.idHotel = idHotel;
	}
	
	
}
