package com.curso.agencia.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.curso.agencia.entity.Hotel;
import com.curso.agencia.service.HotelService;

import io.swagger.annotations.ApiOperation;

/**
 * Controlador del servicio
 * 
 * @author Santiago Velasco
 *
 */
@RestController
@RequestMapping("/hoteles") // http://localhost:8070/hoteles
public class HotelController {

	@Autowired
	HotelService service;

	/**
	 * 
	 * @return lista con todos los hoteles disponibles
	 */
	@ApiOperation(value = "Devuelve una lista con todos los hoteles disponibles")
	@GetMapping
	public ResponseEntity<List<Hotel>> listar() {
		List<Hotel> hoteles = service.listarHotelesDisponibles();
		if (hoteles.isEmpty())
			return ResponseEntity.noContent().build();
		return ResponseEntity.ok(hoteles);
	}

	/**
	 * 
	 * @param nombre del hotel
	 * @return hotel especificado con el nombre
	 */
	@ApiOperation(value = "Devuelve el Hotel con el nombre solicitado")
	@GetMapping(params = "nombre") // http://localhost:8070/hoteles?nombre=elritz
	public ResponseEntity<Hotel> buscarPorNombre(@RequestParam(name = "nombre") String nombre) {
		return ResponseEntity.ok(service.buscarPorNombre(nombre));
	}

	/**
	 * 
	 * @param idHotel identificador del hotel
	 * @return hotel especificado con el identificador
	 */
	@ApiOperation(value = "Devuelve el hotel con el id solicitado")
	@GetMapping("/{idHotel}") // http://localhost:8070/hoteles/1
	public ResponseEntity<Hotel> buscarPorId(@PathVariable Integer idHotel) {
		return ResponseEntity.ok(service.buscarPorId(idHotel));
	}

}
