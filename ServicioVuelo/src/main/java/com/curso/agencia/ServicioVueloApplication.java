package com.curso.agencia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Lanzador del servicio
 * 
 * @author Santiago Velasco
 *
 */
@SpringBootApplication
public class ServicioVueloApplication {

	/**
	 * 
	 * @param args null
	 */
	public static void main(String[] args) {
		SpringApplication.run(ServicioVueloApplication.class, args);
	}

}
