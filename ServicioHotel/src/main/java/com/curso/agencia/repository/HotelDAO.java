package com.curso.agencia.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.curso.agencia.entity.Hotel;

/**
 * Clase DAO para la gestion de Hotel mediante JPA
 * 
 * @author Santiago Velasco
 *
 */
@Repository
public interface HotelDAO extends JpaRepository<Hotel, Integer> {

	/**
	 * 
	 * @return lista de todos los hoteles que esten disponibles
	 */
	@Query("SELECT h FROM Hotel h WHERE h.disponible = true")
	public List<Hotel> listarHotelesDisponibles();
}
