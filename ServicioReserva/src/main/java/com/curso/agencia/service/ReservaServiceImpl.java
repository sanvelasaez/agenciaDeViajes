package com.curso.agencia.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.curso.agencia.entity.Reserva;
import com.curso.agencia.model.Hotel;
import com.curso.agencia.model.ObjetosCombinadosDTO;
import com.curso.agencia.model.Vuelo;
import com.curso.agencia.repository.ReservaDAO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Clase de servicios del microservicio ServicioReserva
 * 
 * @author Santiago Velasco
 *
 */
@Service
public class ReservaServiceImpl implements ReservaService {

	@Autowired
	RestTemplate template;

	@Autowired
	ReservaDAO dao;

	// Controlador hotel
	String urlHotel = "http://localhost:8070/hoteles";
	// Controlador vuelo
	String urlVuelo = "http://localhost:8090/vuelos";

	@Override
	public void hacerReserva(Reserva reserva, Integer totalPersonas) {

		Vuelo vuelo = template.getForObject(urlVuelo + "/" + reserva.getIdVuelo(), Vuelo.class);

		if (vuelo != null && vuelo.getPlazasDisponibles() > totalPersonas) {
			template.put(urlVuelo + "?idVuelo=" + vuelo.getIdVuelo() + "&plazasReservadas=" + totalPersonas, null);
			dao.save(reserva);
		}
	}

	@Override
	public List<ObjetosCombinadosDTO> listarReservas() {

		return combinarObjetos();
	}

	/**
	 * 
	 * @return List(ObjetosCombinadosDTO) devuelve una lista de las reservas
	 *         registradas junto a todos los datos de sus respectivos vuelos y
	 *         hoteles
	 */
	public List<ObjetosCombinadosDTO> combinarObjetos() {

		List<ObjetosCombinadosDTO> combos = new ArrayList<>();

		for (Reserva reserva : dao.findAll()) {

			Vuelo vuelo = template.getForObject(urlVuelo + "/" + reserva.getIdVuelo(), Vuelo.class);
			Hotel hotel = template.getForObject(urlHotel + "/" + reserva.getIdHotel(), Hotel.class);
			combos.add(new ObjetosCombinadosDTO(reserva, vuelo, hotel));
		}

		return combos;
	}

}
