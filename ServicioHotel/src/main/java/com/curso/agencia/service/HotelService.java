package com.curso.agencia.service;

import java.util.List;

import com.curso.agencia.entity.Hotel;

/**
 * Interfaz servicios del microservicio ServicioHotel
 * 
 * @author Santiago Velasco
 *
 */
public interface HotelService {

	/**
	 * 
	 * @return lista de hoteles disponibles
	 */
	List<Hotel> listarHotelesDisponibles();

	/**
	 * 
	 * @param nombre para buscar un hotel especifico
	 * @return el hotel que coincida con el nombre solicitado
	 */
	Hotel buscarPorNombre(String nombre);
	
	/**
	 * 
	 * @param idHotel id del hotel a buscar
	 * @return devuelve el hotel con el id aportado
	 */
	Hotel buscarPorId(Integer idHotel);
}
