package com.curso.agencia.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.curso.agencia.entity.Reserva;
import com.curso.agencia.model.ObjetosCombinadosDTO;
import com.curso.agencia.service.ReservaService;

import io.swagger.annotations.ApiOperation;

/**
 * Controlador del servicio
 * 
 * @author Santiago Velasco
 *
 */
@RestController
@RequestMapping("/reservas") // http://localhost:8080/reservas
public class ReservaController {

	@Autowired
	ReservaService service;

	/**
	 * 
	 * @return lista con todas las reservas junto a los datos de sus respectivos vuelos y hoteles
	 */
	@ApiOperation(value = "Devuelve la lista con todas las reservas junto a los datos de sus respectivos vuelos y hoteles")
	@GetMapping
	public ResponseEntity<List<ObjetosCombinadosDTO>> listarReservas() {
		List<ObjetosCombinadosDTO> reservas = service.listarReservas();
		if (reservas.isEmpty())
			return ResponseEntity.noContent().build();
		return ResponseEntity.ok(reservas);
	}

	/**
	 * 
	 * @param reserva datos de la reserva a registrar
	 * @param totalPersonas numero de personas incluidas en la reserva
	 */
	@ApiOperation(value = "Crea una nueva reserva a partir de un json en el body y actualiza el vuelo con el numero de personas que se le pasa por parametro")
	@PostMapping(params = "totalPersonas") // http://localhost:8080/reservas?totalPersonas=10
	public void hacerReserva(@RequestBody Reserva reserva,
			@RequestParam(name = "totalPersonas") Integer totalPersonas) {
		service.hacerReserva(reserva, totalPersonas);
	}

}
