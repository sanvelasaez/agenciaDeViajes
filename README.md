# Nombre del Proyecto: Agencia de Viajes

Este es el proyecto de la Agencia de Viajes, el cual consiste en tres microservicios interconectados: Hoteles, Vuelos y Reservas. Cada microservicio implementa una API RESTful y se encarga de gestionar la información relacionada con los hoteles, los vuelos y las reservas respectivamente. Todos los microservicios tienen acceso a sus tablas correspondientes en una base de datos MySQL utilizando JPA y Hibernate para la capa de persistencia.

## Descripción del Proyecto

El proyecto se compone de los siguientes microservicios:

### Microservicio de Hoteles
Este microservicio se encarga de gestionar los hoteles. Proporciona funcionalidades como:

- Listar los hoteles disponibles.
- Buscar un hotel por su nombre.
- Buscar un hotel por su identificador único.

### Microservicio de Vuelos
Este microservicio se encarga de gestionar los vuelos. Ofrece las siguientes funcionalidades:

- Filtrar los vuelos disponibles por el número de plazas disponibles.
- Actualizar el número de plazas disponibles de un vuelo después de una reserva.
- Buscar un vuelo por su identificador único.

### Microservicio de Reservas
Este microservicio se encarga de gestionar las reservas y es donde se gestionan todas las operaciones. Ofrece las siguientes funcionalidades:

- Listar todas las reservas realizadas incluyendo los datos del hotel y del vuelo.
- Realizar una reserva para un vuelo y un hotel.

Cada microservicio implementa su propia capa de persistencia utilizando JPA y Hibernate para interactuar con la base de datos MySQL.

## Tecnologías Utilizadas

El proyecto ha sido desarrollado utilizando las siguientes tecnologías y herramientas:

- Spring: Framework de desarrollo de aplicaciones Java utilizado para construir aplicaciones empresariales.
- MySQL: Sistema de gestión de bases de datos relacional utilizado para almacenar la información de hoteles, vuelos y reservas.
- Hibernate JPA: Implementación de la especificación JPA (Java Persistence API) utilizada para el mapeo objeto-relacional y la interacción con la base de datos.
- Swagger2: Biblioteca utilizada para generar documentación interactiva de la API.
- Java: Lenguaje de programación utilizado para implementar la lógica de los microservicios.
- Eclipse: Entorno de desarrollo integrado (IDE) utilizado para desarrollar y administrar el proyecto.

## Configuración del Proyecto

Para configurar y ejecutar el proyecto en tu entorno local, sigue los pasos a continuación:

1. Clona este repositorio en tu máquina local o descárgalo como archivo ZIP.
2. Importa cada microservicio en Eclipse o en tu IDE preferido.
3. Asegúrate de tener MySQL instalado y configurado con una base de datos agencia.
4. Configura las conexiones a la base de datos en los archivos de configuración correspondientes (por ejemplo, `application.properties`), proporcionando la URL de la base de datos, el nombre de usuario y la contraseña si es necesario.
5. Compila y ejecuta cada microservicio por separado. Asegúrate de que no haya errores.
6. Los microservicios estarán disponibles en las URL correspondientes, por ejemplo, `http://localhost:8070/hoteles` para el microservicio de Hoteles, `http://localhost:8090/vuelos` para el microservicio de Vuelos y `http://localhost:8080/reservas` para el microservicio de Reservas.

## Documentación de la API

La documentación de la API se genera automáticamente utilizando Swagger2. Puedes acceder a la documentación interactiva de cada microservicio en las siguientes URL:

- Microservicio de Hoteles: `http://localhost:8070/swagger-ui.html`
- Microservicio de Vuelos: `http://localhost:8090/swagger-ui.html`
- Microservicio de Reservas: `http://localhost:8080/swagger-ui.html`

## Contribución

Si deseas contribuir a este proyecto, puedes seguir los pasos a continuación:

1. Realiza un fork de este repositorio y clónalo en tu máquina local.
2. Crea una rama nueva para realizar tus modificaciones: `git checkout -b feature/nueva-caracteristica`.
3. Realiza las modificaciones y realiza confirmaciones (commits) claros para describir tus cambios.
4. Publica tus cambios en tu repositorio remoto: `git push origin feature/nueva-caracteristica`.
5. Crea una solicitud de extracción (pull request) en este repositorio y describe tus modificaciones detalladamente.

Todas las contribuciones son bienvenidas. Cualquier corrección de errores, mejoras de funcionalidad o nuevas características serán apreciadas.

## Contacto

Si tienes alguna pregunta, sugerencia o problema relacionado con el proyecto, no dudes en contactar al equipo de desarrollo a través del siguiente correo electrónico: [micorreoelectronico@example.com](mailto:tu-correo-electronico@example.com).
