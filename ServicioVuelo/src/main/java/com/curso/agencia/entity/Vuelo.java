package com.curso.agencia.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad vuelo para la gestion por JPA de los mismos
 * 
 * @author Santiago Velasco
 *
 */
@Entity
@Table(name = "vuelo")
public class Vuelo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idVuelo;
	private String compania;
	@Column(columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
	private Timestamp fechaVuelo = new Timestamp(System.currentTimeMillis());
	private Double precio;
	private Integer plazasDisponibles;

	/**
	 * @param idVuelo			identificador del vuelo
	 * @param compania			nombre de la compania aerea
	 * @param fechaVuelo		fecha y hora del vuelo
	 * @param precio			precio del vuelo
	 * @param plazasDisponibles	plazas disponibles en el vuelo
	 */
	public Vuelo(Integer idVuelo, String compania, Timestamp fechaVuelo, Double precio, Integer plazasDisponibles) {
		this.idVuelo = idVuelo;
		this.compania = compania;
		this.fechaVuelo = fechaVuelo;
		this.precio = precio;
		this.plazasDisponibles = plazasDisponibles;
	}

	/**
	 * 
	 */
	public Vuelo() {
	}

	/**
	 * @return the idVuelo
	 */
	public Integer getIdVuelo() {
		return idVuelo;
	}

	/**
	 * @param idVuelo the idVuelo to set
	 */
	public void setIdVuelo(Integer idVuelo) {
		this.idVuelo = idVuelo;
	}

	/**
	 * @return the compania
	 */
	public String getCompania() {
		return compania;
	}

	/**
	 * @param compania the compania to set
	 */
	public void setCompania(String compania) {
		this.compania = compania;
	}

	/**
	 * @return the fechaVuelo
	 */
	public Timestamp getFechaVuelo() {
		return fechaVuelo;
	}

	/**
	 * @param fechaVuelo the fechaVuelo to set
	 */
	public void setFechaVuelo(Timestamp fechaVuelo) {
		this.fechaVuelo = fechaVuelo;
	}

	/**
	 * @return the precio
	 */
	public Double getPrecio() {
		return precio;
	}

	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	/**
	 * @return the plazasDisponibles
	 */
	public Integer getPlazasDisponibles() {
		return plazasDisponibles;
	}

	/**
	 * @param plazasDisponibles the plazasDisponibles to set
	 */
	public void setPlazasDisponibles(Integer plazasDisponibles) {
		this.plazasDisponibles = plazasDisponibles;
	}

}
