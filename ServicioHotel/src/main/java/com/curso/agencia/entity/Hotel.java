package com.curso.agencia.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * Entidad hotel para la gestion por JPA de los mismos
 * 
 * @author Santiago Velasco
 *
 */
@Entity
@Table(name = "hotel")
public class Hotel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idHotel;
	private String nombre;
	private String categoria;
	private Double precio;
	private Boolean disponible;
	/**
	 * @param idHotel 		identificador del hotel
	 * @param nombre		nombre del hotel
	 * @param categoria		categoria del hotel (las estrellas)
	 * @param precio		precio del hotel
	 * @param disponible	disponibilidad del hotel
	 */
	public Hotel(Integer idHotel, String nombre, String categoria, Double precio, Boolean disponible) {
		this.idHotel = idHotel;
		this.nombre = nombre;
		this.categoria = categoria;
		this.precio = precio;
		this.disponible = disponible;
	}
	/**
	 * 
	 */
	public Hotel() {
	}
	/**
	 * @return the idHotel
	 */
	public Integer getIdHotel() {
		return idHotel;
	}
	/**
	 * @param idHotel the idHotel to set
	 */
	public void setIdHotel(Integer idHotel) {
		this.idHotel = idHotel;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the categoria
	 */
	public String getCategoria() {
		return categoria;
	}
	/**
	 * @param categoria the categoria to set
	 */
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	/**
	 * @return the precio
	 */
	public Double getPrecio() {
		return precio;
	}
	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	/**
	 * @return the disponible
	 */
	public Boolean getDisponible() {
		return disponible;
	}
	/**
	 * @param disponible the disponible to set
	 */
	public void setDisponible(Boolean disponible) {
		this.disponible = disponible;
	}
	
	
}
