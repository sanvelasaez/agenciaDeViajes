package com.curso.agencia.service;

import java.util.List;

import com.curso.agencia.entity.Vuelo;

/**
 * Interfaz servicios del microservicio ServicioVuelo
 * 
 * @author Santiago Velasco
 *
 */
public interface VueloService {

	/**
	 * 
	 * @param plazasDisponibles las plazas disponibles que quedan en un vuelo para poder filtrarlos
	 * @return lista de vuelos filtrados por plazas disponibles
	 */
	List<Vuelo> filtrarPorPlazas(Integer plazasDisponibles);

	/**
	 * 
	 * @param idVuelo id del vuelo a modificar
	 * @param plazasReservadas plazas que han sido seleccionadas para ser reservadas
	 */
	void actualizarPlazas(Integer idVuelo, Integer plazasReservadas);
	

	/**
	 * 
	 * @param idVuelo id del vuelo a buscar
	 * @return el vuelo buscado
	 */
	Vuelo buscaVuelo(Integer idVuelo);
}
