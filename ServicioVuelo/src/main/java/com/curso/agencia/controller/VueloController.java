package com.curso.agencia.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.curso.agencia.entity.Vuelo;
import com.curso.agencia.service.VueloService;

import io.swagger.annotations.ApiOperation;

/**
 * Controlador del servicio
 * 
 * @author Santiago Velasco
 *
 */
@RestController
@RequestMapping("/vuelos") // http://localhost:8090/vuelos
public class VueloController {

	@Autowired
	VueloService service;

	/**
	 * 
	 * @param plazasDisponibles del vuelo
	 * @return lista de vuelos filtrados por el numero de plazas disponibles
	 */
	@ApiOperation(value = "Devuelve la lista de vuelos filtrados por el numero de plazas disponibles")
	@GetMapping(params = "plazasDisponibles") // http://localhost:8090/vuelos?plazasDisponibles=15
	public ResponseEntity<List<Vuelo>> filtrarPorPlazas(
			@RequestParam(name = "plazasDisponibles") Integer plazasDisponibles) {
		List<Vuelo> vuelos = service.filtrarPorPlazas(plazasDisponibles);
		if (vuelos.isEmpty())
			return ResponseEntity.noContent().build();
		return ResponseEntity.ok(vuelos);
	}

	/**
	 * 
	 * @param idVuelo          identificador del vuelo a actualizar
	 * @param plazasReservadas numero de plazas a reservar
	 */
	@ApiOperation(value = "Actualiza las plazas disponibles indicando el id del vuelo y las plazas que se van a reservar")
	@PutMapping(params = { "idVuelo", "plazasReservadas" }) // http://localhost:8090/vuelos?idVuelo=1&plazasReservadas=5
	public void actualizarPlazas(@RequestParam(name = "idVuelo") Integer idVuelo,
			@RequestParam(name = "plazasReservadas") Integer plazasReservadas) {
		service.actualizarPlazas(idVuelo, plazasReservadas);
	}

	/**
	 * 
	 * @param idVuelo identificador del vuelo
	 * @return vuelo solicitado por su identificador
	 */
	@ApiOperation(value = "Devuelve el vuelo con el id solicitado")
	@GetMapping("/{idVuelo}") // http://localhost:8090/vuelos/1
	public ResponseEntity<Vuelo> buscaVuelo(@PathVariable(name = "idVuelo") Integer idVuelo) {
		return ResponseEntity.ok(service.buscaVuelo(idVuelo));
	}

}
