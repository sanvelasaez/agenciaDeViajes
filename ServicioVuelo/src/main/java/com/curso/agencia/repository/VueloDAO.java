package com.curso.agencia.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.curso.agencia.entity.Vuelo;

/**
 * Clase DAO para la gestion de Vuelo mediante JPA
 * 
 * @author Santiago Velasco
 *
 */
@Repository
public interface VueloDAO extends JpaRepository<Vuelo, Integer> {

	/**
	 * 
	 * @param plazasDisponibles del vuelo para filtrarlas
	 * @return lista de vuelos filtrados con mas plazas de las solicitadas
	 */
	@Query("SELECT v FROM Vuelo v WHERE v.plazasDisponibles >= :plazasDisponibles")
	List<Vuelo> filtrarPorPlazas(Integer plazasDisponibles);

}
